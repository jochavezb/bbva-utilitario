#! /bin/bash

echo "INICIANDO CREACION DE COLAS EN QMGR " $1;
echo "=================================";

runmqsc $1
DEFINE QLOCAL(QL.GEN.INPUTMQ.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QL.GEN.ERROR.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QL.GEN.AUDIT.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QL.GEN.MEDIUM.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QL.GEN.MEDIUMMQ.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QL.GEN.MERGE.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QL.GEN.TESTIGOMQ.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QL.GEN.OUTPUT.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(IB.QL.RES) CLUSTER($2) MAXDEPTH(50000) REPLACE

DEFINE QLOCAL(QL.GEN.ROUTE.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QL.GEN.EVENTS.HUB.REQ.01) CLUSTER($2) MAXDEPTH(50000) REPLACE

END

echo "ASIGNADO PERMISOS A LAS COLAS DEL QMGR " $1;
echo "=================================";

setmqaut -m $1 -n QL.GEN.INPUTMQ.REQ.01 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QL.GEN.ERROR.REQ.01 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QL.GEN.AUDIT.REQ.01 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QL.GEN.MEDIUM.REQ.01 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QL.GEN.MEDIUMMQ.REQ.01 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QL.GEN.MERGE.REQ.01 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QL.GEN.TESTIGOMQ.REQ.01 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QL.GEN.OUTPUT.REQ.01 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n IB.QL.RES -t queue -p brkuser +put +get +inq

setmqaut -m $1 -n QL.GEN.ROUTE.REQ.01 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QL.GEN.EVENTS.HUB.REQ.01 -t queue -p brkuser +put +get +inq