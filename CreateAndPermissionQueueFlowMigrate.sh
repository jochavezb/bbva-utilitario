#! /bin/bash

echo "INICIANDO CREACION DE COLAS EN QMGR " $1;
echo "=================================";

runmqsc $1
#BMATIC
DEFINE QLOCAL(QRT.BMATC.ENVIO.MPD1) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLT.BMATC.RESP) CLUSTER($2) MAXDEPTH(50000) REPLACE
#COMERCIO
DEFINE QLOCAL(QLT.COMERC.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QRT.UNIQ.RESP.MPD1) CLUSTER($2) MAXDEPTH(50000) REPLACE
#HIDRANDINA
DEFINE QLOCAL(QLT.HIDRA.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QRT.UNIQ.RESP.MPD1) CLUSTER($2) MAXDEPTH(50000) REPLACE
#WONG
DEFINE QLOCAL(QLT.WONG.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QRT.UNIQ.RESP.MPD1) CLUSTER($2) MAXDEPTH(50000) REPLACE
#RECAUDOS
DEFINE QLOCAL(QLP.UNIQ.ENVIO) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.MIRAFLORES.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.UNIQ.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.MSURCO.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.DPWORLD.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.CENCOSUD.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.HIDRANDINA.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.STARPERU.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.PUCP.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.SEAL.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.COMERC.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.UCV.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
#MUNSURCO
DEFINE QLOCAL(QLT.MSURCO.IN) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QRC.UNIQ.RESP.MPD1) CLUSTER($2) MAXDEPTH(50000) REPLACE
#ALERTAS BANCA INTERNET
DEFINE QLOCAL(QLT.SMSC.RECEP) CLUSTER($2) MAXDEPTH(50000) REPLACE
#MONITOR
DEFINE QLOCAL(QRP.MOACF.ENVIO.MPP1) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QLP.MOACF.RESP) CLUSTER($2) MAXDEPTH(50000) REPLACE
DEFINE QLOCAL(QRP.MOACF.ENVIO.MPP1) CLUSTER($2) MAXDEPTH(50000) REPLACE

END

echo "ASIGNADO PERMISOS A LAS COLAS DEL QMGR " $1;
echo "=================================";

setmqaut -m $1 -n QRT.BMATC.ENVIO.MPD1 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLT.BMATC.RESP -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLT.COMERC.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QRT.UNIQ.RESP.MPD1 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLT.HIDRA.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QRT.UNIQ.RESP.MPD1 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLT.WONG.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QRT.UNIQ.RESP.MPD1 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.UNIQ.ENVIO -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.MIRAFLORES.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.UNIQ.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.MSURCO.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.DPWORLD.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.CENCOSUD.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.HIDRANDINA.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.STARPERU.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.PUCP.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.SEAL.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.COMERC.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.UCV.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLT.MSURCO.IN -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QRC.UNIQ.RESP.MPD1 -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLT.SMSC.RECEP -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QLP.MOACF.RESP -t queue -p brkuser +put +get +inq
setmqaut -m $1 -n QRP.MOACF.ENVIO.MPP1 -t queue -p brkuser +put +get +inq




