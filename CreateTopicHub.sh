#! /bin/bash

echo "CREACION DE TOPICO DEL HUB EN EL QMGR " $1;
echo "=================================";

runmqsc $1

DEFINE TOPIC('ace/hub/events') TOPICSTR('ace/hub/events') CLUSTER($2) DESCR('Topico para el hub') REPLACE

DEFINE SUB('HUB_EVENTS') TOPICSTR('ace/hub/events') TOPICOBJ(SYSTEM.BROKER.DEFAULT.SUBPOINT) DEST(QL.GEN.EVENTS.HUB.REQ.01) DESTQMGR($1) PSPROP(RFH2) VARUSER(FIXED) REPLACE

END
